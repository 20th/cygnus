<?php

/**
 * @file
 * Default theme hook implementations.
 */

function theme_calendar($variables) {
  $prepared_days = $variables['days'];
  $year = empty($variables['year']) ? format_date(time(), 'custom', 'Y') : $variables['year'];
  $month = empty($variables['month']) ? format_date(time(), 'custom', 'm') : $variables['month'];

  $today = intval(date('d'));
  $day_of_first = date('N', mktime(0, 0, 0, $month, 1, $year));
  $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
  $month_name = format_date(mktime(0, 0, 0, $month, 1, $year), 'custom', 'F Y');

  $days = array();

  for ($i = 0, $to = $day_of_first - 1; $i < $to; $i++) {
    $days[] = array('data' => '');
  }

  for ($i = 1; $i <= $days_in_month; $i++) {
    $day = isset($prepared_days[$i]) ? $prepared_days[$i] : $i;
    if (!is_array($day)) {
      $day = array('data' => $i);
    }

    // Mark today.
    if ($i == $today) {
      $day = drupal_array_merge_deep($day, array('class' => array('today')));
    }

    $days[] = $day;
  }

  $left_to_fill = count($days) % 7;
  if ($left_to_fill > 0) {
    for ($i = (7 - $left_to_fill); $i > 0; $i--) {
      $days[] = array('data' => '');
    }
  }

  $number_of_weeks = intval(count($days) / 7);
  $weeks = array();
  for ($i = 0; $i < $number_of_weeks; $i++) {
    $week = array_slice($days, ($i * 7), 7);
    for ($j = 5; $j < 7; $j++) {
      $week[$j] = drupal_array_merge_deep($week[$j], array('class' => array('weekend')));
    }
    $weeks[] = $week;
  }

  $header = array(t('Mon'), t('Tue'), t('Wed'), t('Thu'), t('Fri'));
  $header[] = array('data' => t('Sat'), 'class' => array('weekend'));
  $header[] = array('data' => t('Sun'), 'class' => array('weekend'));

  $caption = !empty($variables['caption']) ? $variables['caption'] : $month_name;

  return theme('table', array(
    'header' => $header,
    'rows' => $weeks,
    'attributes' => array('class' => array('calendar')),
    'caption' => !empty($variables['show_caption']) ? $caption : NULL,
  ));
}
