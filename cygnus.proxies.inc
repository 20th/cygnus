<?php

/**
 * @file
 * Proxies for hook implementation.
 *
 * Copyright (C) 2014, Victor Nikulshin
 *
 * This file is part of 'Cygnus' Drupal module.
 *
 * Cygnus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cygnus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cygnus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Proxy for hook_block_info().
 */
function cygnus_proxy_hook_block_info($module, $args = array()) {
  $plugins = ctools_get_plugins('cygnus', 'blocks');
  $blocks = array();

  foreach ($plugins as $plugin) {
    if (empty($plugin['module']) || $plugin['module'] != $module) {
      // Limit list to only one module that performed proxy call.
      continue;
    }

    if (empty($plugin['name'])) {
      continue;
    }
    $name = $plugin['name'];
    $block = array();

    $block['info']  = empty($plugin['info']) ? $name : $plugin['info'];
    $block['cache'] = $plugin['cache'];
    $block['region'] = $plugin['region'];
    $block['status'] = !empty($plugin['region']) ? 1 : 0;
    $block['weight'] = $plugin['weight'];

    $blocks[$name] = $block;
  }

  return $blocks;
}

/**
 * Proxy for hook_block_view().
 */
function cygnus_proxy_hook_block_view($module, $args = array()) {
  $delta = array_shift($args);

  $plugin = NULL;
  foreach (ctools_get_plugins('cygnus', 'blocks') as $candidate) {
    if ($candidate['module'] == $module && $candidate['name'] == $delta) {
      $plugin = $candidate;
    }
  }

  if (empty($plugin) || !($callback = ctools_plugin_get_function($plugin, 'view'))) {
    return array();
  }

  if ($is_visible = ctools_plugin_get_function($plugin, 'is_visible')) {
    if ($is_visible($delta) === FALSE) {
      return array();
    }
  }

  return ($data = $callback($delta)) && is_array($data) ? $data : array();
}

/**
 * Proxy for hook_block_configure().
 */
function cygnus_proxy_hook_block_configure($module, $args = array()) {
  $delta = array_shift($args);

  $plugin = NULL;
  foreach (ctools_get_plugins('cygnus', 'blocks') as $candidate) {
    if ($candidate['module'] == $module && $candidate['name'] == $delta) {
      $plugin = $candidate;
    }
  }

  if (empty($plugin) || !($callback = ctools_plugin_get_function($plugin, 'configure'))) {
    return array();
  }

  return ($data = $callback()) && is_array($data) ? $data : array();
}

/**
 * Proxy for hook_block_save().
 */
function cygnus_proxy_hook_block_save($module, $args = array()) {
  $delta = array_shift($args);
  $edit  = array_shift($args);

  $plugin = NULL;
  foreach (ctools_get_plugins('cygnus', 'blocks') as $candidate) {
    if ($candidate['module'] == $module && $candidate['name'] == $delta) {
      $plugin = $candidate;
    }
  }

  if (empty($plugin) || !($callback = ctools_plugin_get_function($plugin, 'save'))) {
    return array();
  }

  return $callback($edit);
}
