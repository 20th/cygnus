<?php

/**
 * @file
 * A set of helpers to simply direct work with $_GET array.
 */

/**
 * Retrives variable value from _GET array.
 */
function r_get($name) {
  if (!empty($_GET[$name]) && is_string($_GET[$name])) {
    return $_GET[$name];
  }

  return NULL;
}

/**
 * Converts value to a string and shortens its length.
 */
function r_string($string, $max_length = 128) {
  return drupal_substr(strval($string), 0, $max_length);
}

/**
 * Converts value to an integer.
 */
function r_int($int, $positive = TRUE) {
  $int = intval($int);

  return $positive && $int < 0 ? 0 : $int;
}


/**
 * Checks that a node exists and is of specified type.
 */
function r_node($nid, $type = NULL) {
  $node = node_load(r_int($nid));

  if ($node) {
      if ($type && $node->type != $type) {
          return NULL;
      }

      return $node;
  }

  return NULL;
}

/**
 * Extracts nid from a node.
 */
function r_nid($node) {
  return $node && is_object($node) ? $node->nid : NULL;
}

/**
 * Checks that a term exitst and belongs to the specified vocabulary.
 */
function r_term($vocab, $tid) {
  $term = taxonomy_term_load(r_int($tid));

  return $term && $term->vocabulary_machine_name == $vocab ? $term : NULL;
}

/**
 * Extracts tid from term.
 */
function r_tid($term) {
  return $term && is_object($term) ? $term->tid : NULL;
}

/**
 * Ensures that value is an array that contains only allowed values.
 */
function r_array($array, $allowed = array(), $separator = ',') {
  $array = array_filter(explode($separator, $array));

  if ($allowed) {
    $filtered = array();

    foreach ($array as $i) {
      if (in_array($i, $allowed) !== FALSE) {
        $filtered[] = $i;
      }
    }

    return $filtered;
  }

  return $array;
}
