<?php

/**
 * @file
 * Defines module installation helpers.
 *
 * Copyright (C) 2014, Victor Nikulshin
 *
 * This file is part of 'Cygnus' Drupal module.
 *
 * Cygnus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cygnus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cygnus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Configures creation and display properties of node types.
 *
 * Changes values of system variables related to the specified node type.
 */
function cygnus_installer_neutralize_node_settings($type, array $config = array()) {
  $config += array(
    'comments' => FALSE,
    'menu' => array(),
    'preview' => FALSE,
    'publish' => TRUE,
    'revision' => TRUE,
    'submitted' => FALSE,
    'translate' => FALSE,
  );

  if (!preg_match('/^[A-Za-z0-9_-]+$/', $type)) {
    // Invalid type format.
    return FALSE;
  }

  $options = array();
  if ($config['publish']) {
    $options[] = 'status';
  }
  if ($config['revision']) {
    $options[] = 'revision';
  }
  variable_set("node_options_{$type}", $options);

  variable_set("node_preview_{$type}", $config['preview'] ? 1 : 0);
  variable_set("node_submitted_{$type}", $config['submitted'] ? 1 : 0);
  variable_set("menu_options_{$type}", $config['menu']);
  variable_set("comment_{$type}", $config['comments'] ? 2 : 1);
  variable_set("language_content_type_{$type}", $config['translate'] ? 2 : 0);

  return TRUE;
}

/**
 * Creates and updates default views in the caller's directory.
 */
function cygnus_installer_create_views() {
  if (!($caller = _cygnus_lookup_caller())) {
    return;
  }
  $module = $caller[0];

  $path = drupal_get_path('module', $module) . '/views';
  if (is_dir($path)) {
    foreach (file_scan_directory($path, '/\.(php|view)$/') as $file) {
      $view = include $file->uri;

      $old_view = views_get_view($view->name);
      if (!empty($old_view)) {
        views_delete_view($old_view);
      }

      $view->save();
      watchdog('system', format_string('View %n has been created.', array('%n' => $view->name)));
    }
  }
}

/**
 * Creates and updates default entity fields in the caller's directory.
 */
function cygnus_installer_create_fields() {
  if (!($caller = _cygnus_lookup_caller())) {
    return;
  }
  $module = $caller[0];

  $path = drupal_get_path('module', $module) . '/fields';
  $field_dirs = file_scan_directory($path, '/^([0-9]+\.)?[a-z][a-z0-9_]+$/', array('recurse' => FALSE, 'key' => 'filename'));
  ksort($field_dirs);

  foreach ($field_dirs as $field_dir) {
    $field_path = $field_dir->uri;
    $field_name = explode('.', $field_dir->filename);
    $field_name = array_pop($field_name);

    if (!is_file("$field_path/field.php")) {
      continue;
    }

    $field = include "$field_path/field.php";
    if (is_array($field)) {
      $field['field_name'] = $field_name;
      if ($existing = field_info_field($field['field_name'])) {
        field_update_field($field);
      }
      else {
        field_create_field($field);
      }
    }

    foreach (file_scan_directory($field_path, '/^instance\.(.+\.)?php$/') as $instance_file) {
      $instance = include $instance_file->uri;
      if (is_array($instance)) {
        $instance['field_name'] = $field_name;

        if ($existing = field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
          foreach ($instance['display'] as $display_name => $display) {
            if (!empty($existing['display'][$display_name])) {
              $instance['display'][$display_name]['weight'] = $existing['display'][$display_name]['weight'];
            }
          }

          $instance['widget']['weight'] = $existing['widget']['weight'];
          field_update_instance($instance);
        }
        else {
          field_create_instance($instance);
        }
      }
    }
  }
}

/**
 * Creates a taxonomy vocabulary and a set of terms.
 */
function cygnus_installer_create_taxonomy($vocab_name, $human_name, array $items = array()) {
  $vocab = taxonomy_vocabulary_machine_name_load($vocab_name);
  if ($vocab) {
    if ($vocab->name != $human_name) {
      $vocab->name = $human_name;
      taxonomy_vocabulary_save($vocab);
    }
  } else {
    if (!empty($human_name)) {
      $vocab = (object) array(
        'name' => $human_name,
        'machine_name' => $vocab_name,
      );
      taxonomy_vocabulary_save($vocab);
    }
  }

  if (!empty($vocab)) {
    _cygnus_installer_create_taxonomy_helper($vocab, $items);
  }
}

function _cygnus_installer_create_taxonomy_helper($vocab, &$items, $parent = NULL) {
  $existing = taxonomy_get_tree($vocab->vid, $parent ? $parent->tid : 0);

  foreach ($items as $weight => $item) {
    $term = null;
    foreach ($existing as $i) {
      if ($i->name == $item['label']) {
        $term = taxonomy_term_load($i->tid);
        break;
      }
    }

    if (!$term) {
      $term = (object) array(
        'vid' => $vocab->vid,
        'name' => $item['label'],
        'weight' => $weight,
      );

      if ($parent) {
        $term->parent = $parent->tid;
      }

      if (!empty($item['alias'])) {
        $term->path = array('alias' => $item['alias']);
      }

      taxonomy_term_save($term);
    }

    $items[$weight]['term'] = $term;

    if (!empty($item['items'])) {
      _cygnus_installer_create_taxonomy_helper($vocab, $item['items'], $term);
    }
  }
}

function cygnus_installer_create_uc_attributes(array $options) {
  if (!module_exists('uc_attribute')) {
    return;
  }

  $attributes = db_query('SELECT name, aid FROM {uc_attributes}')->fetchAllKeyed();

  foreach (array_keys($options) as $ordeting => $name) {
    if (isset($options[$name]['display'])) {
      $display = (int) $options[$name]['display'];
      unset($options[$name]['display']);
    }
    else {
      $display = 1;
    }

    if (isset($options[$name]['required'])) {
      $required = (bool) $options[$name]['required'];
      unset($options[$name]['required']);
    }
    else {
      $required = TRUE;
    }

    if (empty($attributes[$name])) {
      $attribute = (object) array(
        'name' => $name,
        'label' => $name,
        'ordering' => $ordering,
        'required' => $required,
        'display' => $display,
        'description' => '',
      );

      uc_attribute_save($attribute);
      $attributes[$name] = $attribute->aid;
    }
  }

  foreach ($attributes as $name => $aid) {
    $attribute = uc_attribute_load($aid);

    foreach ($attribute->options as $option) {
      $options[$name] = array_diff($options[$name], array($option->name));
    }
  }

  foreach ($options as $name => $items) {
    foreach ($items as $ordering => $item) {
      $option = (object) array(
        'aid' => $attributes[$name],
        'name' => $item,
        'ordering' => $ordering,
      );

      uc_attribute_option_save($option);
    }
  }
}
