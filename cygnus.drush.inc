<?php

/**
 * @file
 * Drush command file.
 */

/**
 * Implements hook_drush_command().
 */
function cygnus_drush_command() {
  $items['export-field-instance'] = array(
    'description' => dt('Creates an field instance dump in Cygnus format.'),
    'arguments' => array(
      'entity_type' => dt('Entity type to load instance.'),
      'field_name' => dt('Name of exported field.'),
      'bundle' => dt('Bundle of exported field instance.'),
    ),
    'aliases' => array('efi'),
  );

  return $items;
}

/**
 * Drush command callback.
 */
function drush_cygnus_export_field_instance($entity_type, $field_name, $bundle) {
  require DRUPAL_ROOT . '/includes/utility.inc';

  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  if (!$field || !$instance) {
    drush_log('Specified field instance does not exist.', 'error');
    return;
  }

  $export_name = preg_replace('/^field_/', '', $field_name);

  $path = drush_cwd() . '/' . $export_name;
  file_prepare_directory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  $field = array_diff_key($field, array_flip(array(
    'bundles', 'columns', 'deleted', 'foreign keys', 'id', 'indexes',
    'module', 'storage',
  )));
  $field = _drush_cygnus_export_field_instance_sort($field);

  $field['active'] = intval($field['active']) ? TRUE : FALSE;
  $field['cardinality'] = intval($field['cardinality']);
  $field['field_name'] = $export_name;
  $field['locked'] = intval($field['locked']) ? TRUE : FALSE;
  $field['translatable'] = intval($field['translatable']) ? TRUE : FALSE;

  $field_export = "<?php\n\n\$field = " . drupal_var_export($field) . ";\n\nreturn \$field;\n";
  $field_export = str_replace('\'cardinality\' => -1,', '\'cardinality\' => FIELD_CARDINALITY_UNLIMITED,', $field_export);
  file_put_contents("$path/field.php", $field_export);

  $instance = array_diff_key($instance, array_flip(array(
    'deleted', 'field_id', 'id',
  )));
  $instance = _drush_cygnus_export_field_instance_sort($instance);
  $instance = _drush_cygnus_export_field_instance_unset_displaywidget_property($instance, 'module');
  $instance = _drush_cygnus_export_field_instance_unset_displaywidget_property($instance, 'weight');

  $instance['field_name'] = $export_name;
  $instance['required'] = intval($instance['required']) ? TRUE : FALSE;

  $instance_export = "<?php\n\n\$instance = " . drupal_var_export($instance) . ";\n\nreturn \$instance;\n";
  file_put_contents("$path/instance.$bundle.php", $instance_export);

  drush_log('Field exported.', 'ok');
}

/**
 * Recursevily sorts array by keys.
 */
function _drush_cygnus_export_field_instance_sort($array) {
  ksort($array);

  foreach ($array as $key => $value) {
    if (is_array($value)) {
      $array[$key] = _drush_cygnus_export_field_instance_sort($value);
    }
  }

  return $array;
}

/**
 * Removes not usefull properties in display and widget configuration.
 */
function _drush_cygnus_export_field_instance_unset_displaywidget_property($instance, $property) {
  if (isset($instance['widget'][$property])) {
    unset($instance['widget'][$property]);
  }
  foreach ($instance['display'] as $name => $display) {
    if (isset($display[$property])) {
      unset($instance['display'][$name][$property]);
    }
  }

  return $instance;
}
